import 'package:flutter/material.dart';

class ShowPassword extends ChangeNotifier {
  bool _isShown = true;
  bool _registerIsShown = true;

  void trigger() {
    _isShown = !_isShown;
    notifyListeners();
  }

  void triggerRegister() {
    _registerIsShown = !_registerIsShown;
    notifyListeners();
  }

  bool get getValue => _isShown;
  bool get getValueRegister => _registerIsShown;
}
