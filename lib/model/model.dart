class ModelLogin {
  final String? token;
  final String? error;
  ModelLogin({this.token, this.error});

  factory ModelLogin.fromJson(Map<String, dynamic> json) {
    return ModelLogin(
      token: json['token'],
      error: json['error'],
    );
  }

  @override
  String toString() {
    return token.toString();
  }
}
