import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    Key? key,
    required this.hintText,
    required this.textEditingController,
    required this.icon,
    required this.hide,
    this.isShown = false,
    this.needIcon = false,
    this.onPressedSuffix,
    // this.suffix,
  }) : super(key: key);

  final String hintText;
  final TextEditingController textEditingController;
  final IconData icon;
  final bool hide;
  // final IconButton? suffix;
  final bool needIcon;
  final bool? isShown;
  final void Function()? onPressedSuffix;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: TextField(
        controller: textEditingController,
        obscureText: hide,
        cursorColor: Colors.purple,
        decoration: InputDecoration(
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide.none,
          ),
          focusedBorder: const UnderlineInputBorder(
            borderSide: BorderSide.none,
          ),
          hintText: hintText,
          hintStyle: const TextStyle(overflow: TextOverflow.ellipsis),
          filled: true,
          fillColor: const Color(0xffE6E8EF),
          prefixIcon: Icon(icon, color: Colors.purple),
          suffixIcon: needIcon
              ? IconButton(
                  onPressed: onPressedSuffix,
                  icon: isShown!
                      ? const Icon(
                          Icons.visibility_off,
                          color: Colors.purple,
                        )
                      : const Icon(
                          Icons.visibility,
                          color: Colors.purple,
                        ),
                )
              : null,
          hoverColor: Colors.purple,
        ),
      ),
    );
  }
}
