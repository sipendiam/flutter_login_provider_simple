import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_provider/constant/assets_path.dart';

class ApplicationPicture extends StatelessWidget {
  const ApplicationPicture({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      AssetsPath.iconUser,
      semanticsLabel: 'Icon User',
      width: 150,
    );
  }
}
