import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        children: [
          const TextSpan(
            text: 'Don\'t have an account? ',
          ),
          TextSpan(
            recognizer: TapGestureRecognizer()..onTap = onPressed,
            style: const TextStyle(
              color: Colors.purple,
            ),
            text: 'Register Now',
          )
        ],
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    required this.onPressed,
    required this.text,
  }) : super(key: key);

  final Function()? onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: onPressed,
        child: Text(text),
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.purple)),
      ),
    );
  }
}

class ForgetPassword extends StatelessWidget {
  const ForgetPassword({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: const Text(
        'Forgot Password',
        style: TextStyle(
          color: Colors.purple,
        ),
      ),
    );
  }
}
