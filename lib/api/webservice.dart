import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:login_provider/core/navigation.dart';
import 'package:login_provider/model/model.dart';

class API {
  static doLogin(
    BuildContext context,
    Widget to,
    String mailcontroller,
    String passwordcontroller,
  ) async {
    var resp = await API.loginPost('/api/login', {
      "email": mailcontroller,
      "password": passwordcontroller,
    });
    final snackBar = SnackBar(
      content: Text(resp.error ?? ''),
      duration: const Duration(seconds: 2),
    );
    if (resp.error != null) {
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
    if (resp.token != null) {
      GoTo.navigateToWithoutBack(context, to);
    }
  }

  static doRegister(
    BuildContext context,
    Widget to,
    String mailcontroller,
    String passwordcontroller,
  ) async {
    var resp = await API.loginPost('/api/register', {
      "email": mailcontroller,
      "password": passwordcontroller,
    });
    final snackBar = SnackBar(
      content: resp.token != null
          ? const Text('Account created!')
          : Text(resp.error!),
      duration: const Duration(seconds: 2),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    await Future.delayed(const Duration(milliseconds: 1500));
    if (resp.token != null) {
      GoTo.navigateToWithoutBack(context, to);
    }
  }

  static Future<ModelLogin> loginPost(
      String url, Map<String, Object> param) async {
    var link = Uri.parse('https://reqres.in' + url);

    var response = await http.post(link, body: param);
    // log(response.statusCode.toString());
    var logStr = 'Log Start\n\n';
    logStr += 'Status response code = ${response.statusCode}\n\n';
    logStr += 'Parameter\n';
    logStr += '$param \n\n';
    logStr += 'Reponse from web\n';
    logStr += response.body;
    logStr += '\n\nLog End';
    log(logStr);
    if (response.statusCode == 200) {
      // print(ModelLogin.fromJson(jsonDecode(response.body)));
      return ModelLogin.fromJson(jsonDecode(response.body));
    } else {
      // throw Exception('Failed to load data');
      return ModelLogin.fromJson(jsonDecode(response.body));
    }
  }
}
