import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:login_provider/constant/assets_path.dart';
import 'package:login_provider/pages/login_screen.dart';
import 'package:login_provider/widget/button.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SvgPicture.asset(
                AssetsPath.welcomeUser,
                width: 150,
              ),
              CustomButton(
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => LoginScreen()),
                      (route) => false);
                },
                text: 'Log out',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
