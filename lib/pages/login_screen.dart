import 'package:flutter/material.dart';
import 'package:login_provider/core/navigation.dart';
import 'package:login_provider/pages/home_screen.dart';
import 'package:login_provider/api/webservice.dart';
import 'package:login_provider/pages/register_screen.dart';
import 'package:login_provider/provider/show_password.dart';
import 'package:login_provider/widget/button.dart';
import 'package:login_provider/widget/picture.dart';
import 'package:login_provider/widget/textform.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final TextEditingController _emailC = TextEditingController();
  final TextEditingController _passC = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ShowPassword>(context);
    return Scaffold(
        body: Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            const ApplicationPicture(),
            const SizedBox(height: 16),
            const Text('Welcome to \$App.name'),
            const SizedBox(height: 8),
            const Text('\$App.slogan'),
            const SizedBox(height: 16),
            CustomTextField(
              textEditingController: _emailC,
              hintText: 'Email',
              icon: Icons.email,
              hide: false,
            ),
            CustomTextField(
              hintText: 'Password',
              textEditingController: _passC,
              icon: Icons.lock_outline,
              needIcon: true,
              isShown: provider.getValue,
              onPressedSuffix: () {
                provider.trigger();
              },
              hide: provider.getValue,
            ),
            CustomButton(
              text: 'Login',
              onPressed: () {
                API.doLogin(
                    context, const HomePage(), _emailC.text, _passC.text);
              },
            ),
            ForgetPassword(
              onPressed: () {
                const snackBar = SnackBar(
                  content: Text('Doesn\'t Support yet'),
                  duration: Duration(seconds: 2),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
            ),
            const Spacer(),
            RegisterButton(
              onPressed: () {
                GoTo.navigate(context, RegisterScreen());
              },
            ),
            const SizedBox(height: 16),
          ],
        ),
      ),
    ));
  }
}
