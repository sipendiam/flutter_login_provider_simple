import 'package:flutter/material.dart';
import 'package:login_provider/api/webservice.dart';
import 'package:login_provider/pages/login_screen.dart';
import 'package:login_provider/provider/show_password.dart';
import 'package:login_provider/widget/button.dart';
import 'package:login_provider/widget/textform.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatelessWidget {
  RegisterScreen({Key? key}) : super(key: key);

  final TextEditingController _emailC = TextEditingController();
  final TextEditingController _passC = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ShowPassword>(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomTextField(
              hintText: 'Input valid email address',
              textEditingController: _emailC,
              icon: Icons.email,
              hide: false,
            ),
            CustomTextField(
              hintText: 'Create memorable password',
              textEditingController: _passC,
              icon: Icons.lock_outline,
              hide: provider.getValueRegister,
              needIcon: true,
              isShown: provider.getValueRegister,
              onPressedSuffix: () {
                provider.triggerRegister();
              },
            ),
            CustomButton(
              onPressed: () {
                API.doRegister(
                    context, LoginScreen(), _emailC.text, _passC.text);
              },
              text: 'Register',
            ),
          ],
        ),
      ),
    );
  }
}
