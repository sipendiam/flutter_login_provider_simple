import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_svg/svg.dart';
import 'package:login_provider/constant/assets_path.dart';
import 'package:login_provider/core/navigation.dart';
import 'package:login_provider/pages/login_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    _initSplash();
    super.initState();
  }

  _initSplash() async {
    FlutterNativeSplash.remove();
    await Future.delayed(const Duration(milliseconds: 1500));
    GoTo.navigateToWithoutBack(context, LoginScreen());
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width / 2;
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: SafeArea(
          child: Center(
            child: SvgPicture.asset(
              AssetsPath.splashImage,
              width: width,
            ),
          ),
        ),
      ),
    );
  }
}
