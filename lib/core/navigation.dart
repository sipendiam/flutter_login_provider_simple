import 'package:flutter/material.dart';

class GoTo {
  static navigateToWithoutBack(BuildContext context, Widget to) {
    Navigator.pushAndRemoveUntil(
        context, MaterialPageRoute(builder: (_) => to), (route) => false);
  }

  static navigate(BuildContext context, Widget to) {
    Navigator.push(context, MaterialPageRoute(builder: (_) => to));
  }
}
