# Flutter_Simple_Login
> FLutter Login page with simple provider

I create this project to give example and archive my work for using provider written in dart and flutter framework.

## Installing / Getting started
---
First you clone this project to your local machine and make sure flutter is installed in your machine,
Open up your emulator or simulator

```shell
cd login_provider
flutter run
```

You should see the application is running on your emulator/simulator.

## Developing
---
Here's a brief intro about what a you must do in order to start developing
the project further:

```shell
cd login_provider
flutter clean
flutter get packages
```

## Features
---
What's all the bells and whistles this project can perform?
* Feel free to copy this project to your own or for your reference
* You can also add and remove any feature you want
* Feel free to link this project, or not I don't really care


## My Todo Next
---
1. Rapihin codebase
2. Centered navigation

###### [Design Inspiration](https://dribbble.com/shots/15635034-Saifty-GDPR-Settings)